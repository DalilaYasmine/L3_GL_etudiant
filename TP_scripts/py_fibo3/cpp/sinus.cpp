#include <cmath>       /* sin */

double sinusf(double a, double b, double n) {
    return sin(2*M_PI*(a*n+b));
}

#include <pybind11/pybind11.h>
PYBIND11_PLUGIN(sinus) {
    pybind11::module m("sinus");
    m.def("sinusf", &sinusf);
    return m.ptr();
}
