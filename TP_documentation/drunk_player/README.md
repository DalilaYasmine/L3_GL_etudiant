# Drunk_player

## Description


Drunk_player est un systeme de lecture de video qi a trop bu. Il lit les videos contenues dans un dossier par morceaux, aleatoirement et parfois en transformant l'image.

Drunk_player utilise la bibliotheque de traitement d'image OpenCV et est compose :

	* d'une bibliotheque (drunk_player) contenant le code de base
	* d'un programme graphique (drunk_player_gui) qui affiche le resultatà l'ecran
	* d'un programme console (drunk_player_cli) qui sort le resultat dans un fichier   

## Dependances 

	OpenCV
	Boost
	
## Compilation


```
 mkdir build
 cd build
 cmake ..
 make
```
 
# Utilisation


./drunk_player_gui.ou ../data/

![drunk_player_gui](drunk_player_gui.png)

