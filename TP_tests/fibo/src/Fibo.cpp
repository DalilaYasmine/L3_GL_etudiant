#include "Fibo.hpp"
#include <assert.h>
#include <cassert>

int fibo(int n, int f0, int f1) {
  assert (f1>=f0);
  assert (f0>=0);
    return n<=0 ? f0 : fibo(n-1, f1, f1+f0);
}

