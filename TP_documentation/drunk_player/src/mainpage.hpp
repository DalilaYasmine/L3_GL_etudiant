/// \mainpage Documentation de drunk_player
/// Drunk_player est un systeme de lecture de video qi a trop bu. Il lit les videos contenues dans un dossier par morceaux, aleatoirement et parfois en transformant l'image.
///
/// Drunk_player utilise la bibliotheque de traitement d'image OpenCV et est compose : 
///
///		-d'une bibliotheque (drunk_player) contenant le code de base
///		-d'un programme graphique (drunk_player_gui) qui affiche le resultatà l'ecran
///		-d'un programme console (drunk_player_cli) qui sort le resultat dans un fichier 
