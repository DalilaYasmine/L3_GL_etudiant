#include "Fibo.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(Fibo)
{

};

TEST(Fibo,test1)
{
  CHECK_EQUAL(fibo(2,0,1),1);
  CHECK_EQUAL(fibo(3,0,1),2);
  CHECK_EQUAL(fibo(4,0,1),3);
}
