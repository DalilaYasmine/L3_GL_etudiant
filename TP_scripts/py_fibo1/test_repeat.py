import repeat
import fibo
 
def print_fibo(n):
	repeat.repeatN(n,fibo.fiboIterative)

print_fibo(10)
